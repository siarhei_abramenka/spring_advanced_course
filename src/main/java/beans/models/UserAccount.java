package beans.models;

public class UserAccount {

    private Double amount;

    public UserAccount(Double amount) {
        this.amount = amount;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }
}
