package beans.daos;

import beans.models.User;

public interface UserAccountDAO {

    void plusToAmount(User user, double amount);

    void subtractFromAmount(User user, double amount);

    double getCurrentAmount(User user);
}
