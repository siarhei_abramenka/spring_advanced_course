package beans.daos.db;

import beans.daos.AbstractDAO;
import beans.daos.UserAccountDAO;
import beans.models.User;
import org.springframework.stereotype.Repository;

@Repository(value = "userAccountDAO")
public class UserAccountDAOImpl extends AbstractDAO implements UserAccountDAO {


    @Override
    public void plusToAmount(User user, double amount) {
        double newAmount = user.getAmount() + amount;
        getCurrentSession().update("AMOUNT", newAmount);
    }

    @Override
    public void subtractFromAmount(User user, double amount) {
        double newAmount = user.getAmount() - amount;
        getCurrentSession().update("AMOUNT", newAmount);
    }

    @Override
    public double getCurrentAmount(User user) {
        return user.getAmount();
    }
}
