package beans.controllers;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import beans.models.Auditorium;
import beans.models.Event;
import beans.models.Ticket;
import beans.models.User;
import beans.services.AuditoriumService;
import beans.services.BookingService;
import beans.services.EventService;
import beans.services.UserService;

@Controller
public class BookingController {

    private BookingService bookingService;
    private EventService eventService;
    private UserService userService;
    private AuditoriumService auditoriumService;

    private static LocalDateTime convertStringToDate(String date) {
        StringBuilder newDate = new StringBuilder(date);
        newDate.append(" 00:00:00:00");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss:nn");
        LocalDateTime dateTime = LocalDateTime.parse(newDate, formatter);
        return dateTime;
    }

    @Autowired
    public BookingController(BookingService bookingService, EventService eventService, UserService userService,
                             AuditoriumService auditoriumService) {
        this.bookingService = bookingService;
        this.eventService = eventService;
        this.userService = userService;
        this.auditoriumService = auditoriumService;
    }

    @RequestMapping(value = "/bookTicket", method = RequestMethod.POST)
    public String bookTicket(@RequestParam(value = "email") String email,
                             @RequestParam(value = "event") String eventName,
                             @RequestParam(value = "date") String date,
                             @RequestParam(value = "auditorium") String auditoriumName) {

        LocalDateTime dateTime = convertStringToDate(date);

        final User user = userService.getUserByEmail(email);
        final Auditorium auditorium = auditoriumService.getByName(auditoriumName);
        final String seats = String.valueOf(auditoriumService.getSeatsNumber(auditoriumName));
        final Event event = eventService.getEvent(eventName, auditorium, dateTime);
        final Ticket newTicket = new Ticket(1, event, dateTime, seats, user, event.getBasePrice());
        bookingService.bookTicket(user, newTicket);
        return "redirect:/index.html";
    }
}
