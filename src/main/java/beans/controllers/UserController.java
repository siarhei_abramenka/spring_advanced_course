package beans.controllers;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import beans.models.Role;
import beans.models.User;
import beans.services.UserService;

@Controller
public class UserController {

    private UserService userService;
    private PasswordEncoder passwordEncoder;

    private static LocalDate convertStringToDate(String date) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate dateTime = LocalDate.parse(date, formatter);
        return dateTime;
    }

    @Autowired
    public UserController(UserService userService, PasswordEncoder passwordEncoder) {
        this.userService = userService;
        this.passwordEncoder = passwordEncoder;
    }

    @RequestMapping(value = "/admin/removeUser", method = RequestMethod.POST)
    public String removeUser(@RequestParam(value = "email") String email) {
        User user = userService.getUserByEmail(email);
        userService.remove(user);
        return "redirect:/index.html";
    }

    @RequestMapping(value = "/user/getUserByEmail", method = RequestMethod.POST)
    public ModelAndView getUserByEmail(@RequestParam(value = "email") String email) {
        final User user = userService.getUserByEmail(email);
        return new ModelAndView("getUserByEmail", "user", user);
    }

    @RequestMapping(value = "/admin/getUserById", method = RequestMethod.POST)
    public ModelAndView getUserById(@RequestParam(value = "id") String id) {
        final long userId = Integer.valueOf(id);
        final User user = userService.getUserById(userId);
        return new ModelAndView("getUserById", "user", user);
    }

    @RequestMapping(value = "/admin/registerNewUser", method = RequestMethod.POST)
    public String registerNewUser(@RequestParam(value = "email") String email,
                                        @RequestParam(value = "name") String name,
                                        @RequestParam(value = "birthday") String birthday,
                                        @RequestParam(value = "password") String password) {
        LocalDate date = convertStringToDate(birthday);
        User user = new User(email, name, date, passwordEncoder.encode(password), Role.ROLE_RESGISTERED_USER, true, 0.0);
        userService.register(user);
        return "redirect:/logout";
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public ModelAndView loginPage() {
        return new ModelAndView("login");
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logoutPage() {
        return "redirect:/index.html";
    }
}
