package beans.controllers;

import beans.exceptions.FileNotFoundExceprion;
import beans.models.Event;
import beans.models.User;
import beans.services.EventService;
import beans.services.UserService;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;
import java.util.Collection;

@Controller
public class UploadFileController {

    private UserService userService;
    private EventService eventService;

    @Autowired
    public UploadFileController(UserService userService, EventService eventService) {
        this.eventService = eventService;
        this.userService = userService;
    }

    @RequestMapping(value = "/uploadFile", method = RequestMethod.POST)
    public String uploadJSONFile(@RequestParam(value = "file") MultipartFile file) throws IOException {
        if (!file.isEmpty()) {

            byte[] bytes = file.getBytes();
            String jsonString = new String(bytes);

            Gson gson = new Gson();
            StoreObjects objects = gson.fromJson(jsonString, StoreObjects.class);

            Collection<User> users = objects.getUsers();
            for (User user : users) {
                userService.register(user);
            }

            Collection<Event> events = objects.getEvents();
            for (Event event : events) {
                eventService.create(event);
            }
            return "redirect:/index.html";
        } else {
            throw new FileNotFoundExceprion("File wasn't found. Please check your file!");
        }

    }

    @ExceptionHandler(FileNotFoundExceprion.class)
    public ModelAndView handleCustomException(FileNotFoundExceprion ex) {

        ModelAndView model = new ModelAndView("customError");
        model.addObject("errMsg", ex.getMessage());
        return model;

    }

    private static class StoreObjects {
        Collection<User> users;
        Collection<Event> events;

        public StoreObjects(Collection<User> users, Collection<Event> events) {
            this.events = events;
            this.users = users;
        }

        public Collection<User> getUsers() {
            return users;
        }

        public void setUsers(Collection<User> users) {
            this.users = users;
        }

        public Collection<Event> getEvents() {
            return events;
        }

        public void setEvents(Collection<Event> events) {
            this.events = events;
        }
    }
}
