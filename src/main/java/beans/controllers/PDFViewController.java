package beans.controllers;

import beans.PDFViews.TicketsForEventPDF;
import beans.PDFViews.TicketsForUserPDF;
import beans.PDFViews.UsersByNamePDF;
import beans.models.Booking;
import beans.models.Ticket;
import beans.models.User;
import beans.services.BookingService;
import beans.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Controller
public class PDFViewController {

    private UserService userService;
    private BookingService bookingService;

    private static LocalDateTime convertStringToDate(String date) {
        StringBuilder newDate = new StringBuilder(date);
        newDate.append(" 00:00:00:00");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss:nn");
        LocalDateTime dateTime = LocalDateTime.parse(newDate, formatter);
        return dateTime;
    }

    @Autowired
    public PDFViewController(UserService userService, BookingService bookingService) {
        this.bookingService = bookingService;
        this.userService = userService;
    }

    @RequestMapping(value = "/getUsersByName", method = RequestMethod.POST)
    public ModelAndView getUsersByName(@RequestParam(value = "name") String name) {
        final List<User> users = userService.getUsersByName(name);
        return new ModelAndView(new UsersByNamePDF(), "users", users);
    }


    @RequestMapping(value = "/getTicketsForUserByEmail", method = RequestMethod.POST)
    public ModelAndView getTicketForUserByEmail(@RequestParam(value = "email") String email) {
        final User user = userService.getUserByEmail(email);
        final List<Ticket> ticketsForUser = userService.getBookedTickets(user);
        return new ModelAndView(new TicketsForUserPDF(), "tickets", ticketsForUser);
    }


    @RequestMapping(value = "/admin/getTicketsForEvent", method = RequestMethod.POST)
    public ModelAndView getTicketsForEvent(@RequestParam(value = "event") String event,
                                           @RequestParam(value = "auditorium") String auditorium,
                                           @RequestParam(value = "date") String date) {

        LocalDateTime dateTime = convertStringToDate(date);

        final List<Ticket> ticketsForEvent = bookingService.getTicketsForEvent(event, auditorium, dateTime);
        return new ModelAndView(new TicketsForEventPDF(), "tickets", ticketsForEvent);
    }
}
