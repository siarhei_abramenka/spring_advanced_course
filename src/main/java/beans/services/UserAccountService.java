package beans.services;

import beans.models.User;

public interface UserAccountService {

    void plusDeposit(User user, Double amount);

    void minusDeposit(User user, Double amount);

    double getAmountOfUser(User user);
}
