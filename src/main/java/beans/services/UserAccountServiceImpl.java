package beans.services;

import beans.daos.UserAccountDAO;
import beans.models.User;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("userAccountServiceImpl")
public class UserAccountServiceImpl implements UserAccountService{

    private final UserAccountDAO userAccountDAO;

    public UserAccountServiceImpl(@Qualifier("userAccountDAO") UserAccountDAO userAccountDAO) {
        this.userAccountDAO = userAccountDAO;
    }

    @Override
    public void plusDeposit(User user, Double amount) {
        userAccountDAO.plusToAmount(user, amount);
    }

    @Override
    public void minusDeposit(User user, Double amount) {
        userAccountDAO.subtractFromAmount(user, amount);
    }

    @Override
    public double getAmountOfUser(User user) {
        return userAccountDAO.getCurrentAmount(user);
    }
}
