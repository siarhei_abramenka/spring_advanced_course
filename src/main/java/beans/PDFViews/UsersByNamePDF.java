package beans.PDFViews;

import beans.models.User;
import com.lowagie.text.Document;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.view.document.AbstractPdfView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

@Service
public class UsersByNamePDF extends AbstractPdfView{

    @Override
    protected void buildPdfDocument(Map<String, Object> model,
                                    Document document,
                                    PdfWriter pdfWriter,
                                    HttpServletRequest httpServletRequest,
                                    HttpServletResponse httpServletResponse) throws Exception {

        List<User> users = (List<User>) model.get("users");
        PdfPTable pdfTable = new PdfPTable(4);
        pdfTable.addCell("ID");
        pdfTable.addCell("Email");
        pdfTable.addCell("Name");
        pdfTable.addCell("Birthday");
        for(User user : users) {
            pdfTable.addCell(String.valueOf(user.getId()));
            pdfTable.addCell(String.valueOf(user.getEmail()));
            pdfTable.addCell(String.valueOf(user.getName()));
            pdfTable.addCell(String.valueOf(user.getBirthday()));
        }
        document.add(pdfTable);
    }
}
