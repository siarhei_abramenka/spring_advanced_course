package beans.PDFViews;

import beans.models.Ticket;
import com.lowagie.text.Document;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.view.document.AbstractPdfView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

@Service
public class TicketsForUserPDF extends AbstractPdfView {

    @Override
    protected void buildPdfDocument(Map<String, Object> model,
                                    Document document,
                                    PdfWriter pdfWriter,
                                    HttpServletRequest httpServletRequest,
                                    HttpServletResponse httpServletResponse) throws Exception {

        List<Ticket> tickets = (List<Ticket>) model.get("tickets");
        PdfPTable pdfTable = new PdfPTable(6);
        pdfTable.addCell("ID");
        pdfTable.addCell("Event");
        pdfTable.addCell("Date");
        pdfTable.addCell("Seats");
        pdfTable.addCell("User");
        pdfTable.addCell("Price");
        for(Ticket ticket : tickets) {
            pdfTable.addCell(String.valueOf(ticket.getId()));
            pdfTable.addCell(String.valueOf(ticket.getEvent().getName()));
            pdfTable.addCell(String.valueOf(ticket.getDateTime()));
            pdfTable.addCell(String.valueOf(ticket.getSeats()));
            pdfTable.addCell(String.valueOf(ticket.getUser().getName()));
            pdfTable.addCell(String.valueOf(ticket.getPrice()));
        }
        document.add(pdfTable);
    }
}
