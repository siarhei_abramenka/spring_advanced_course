package beans.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value= HttpStatus.NOT_FOUND, reason="No such file")
public class FileNotFoundExceprion extends RuntimeException {

    public FileNotFoundExceprion() {
        super();
    }
    public FileNotFoundExceprion(String message, Throwable cause) {
        super(message, cause);
    }
    public FileNotFoundExceprion(String message) {
        super(message);
    }
    public FileNotFoundExceprion(Throwable cause) {
        super(cause);
    }
}
